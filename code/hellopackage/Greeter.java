package hellopackage;
import java.util.Scanner;

import secondpackage.Utilities;
//import java.util.*;
//WE CANNOT DO java.*.*;

public class Greeter {
    public static void main(String args[]){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("enter an integer");
        int userInput = keyboard.nextInt();

        // instance of utilities class
        Utilities util = new Utilities();
        System.out.println(util.doubleMe(userInput));
    }
}